package config

import "github.com/gnames/gnsys"

type Config struct {
	// DataDir describes where data root is located
	DataDir string

	// CurrentCards is the name of the current set of cards, if not given,
	// it is "default" set.
	CurrentCards string

	// StableOnly -- only stable words.
	StableOnly bool

	// OldOnly -- only vocabulary words.
	OldOnly bool

	// NewOnly -- only learning words.
	NewOnly bool

	// CardsSets provides all created card sets. Only "default" set exists
	// from the start.
	CardsSets map[string]struct{}
}

type Option func(*Config)

func OptDataDir(s string) Option {
	return func(cnf *Config) {
		cnf.DataDir = s
	}
}

func OptCurrentCards(s string) Option {
	return func(cnf *Config) {
		cnf.CurrentCards = s
	}
}

func OptStableOnly(b bool) Option {
	return func(cnf *Config) {
		cnf.StableOnly = b
	}
}

func OptOldOnly(b bool) Option {
	return func(cnf *Config) {
		cnf.OldOnly = b
	}
}

func OptNewOnly(b bool) Option {
	return func(cnf *Config) {
		cnf.NewOnly = b
	}
}

func OptCardsSets(sets []string) Option {
	return func(cnf *Config) {
		res := make(map[string]struct{})
		for _, v := range sets {
			res[v] = struct{}{}
		}
		cnf.CardsSets = res
	}
}

func New(opts ...Option) Config {
	dir, _ := gnsys.ConvertTilda("~/.local/share/vorto")
	cnf := Config{
		DataDir:      dir,
		CurrentCards: "default",
		CardsSets:    map[string]struct{}{"default": {}},
	}
	for _, opt := range opts {
		opt(&cnf)
	}
	return cnf
}

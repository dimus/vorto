package entity

type BinType int

const (
	New BinType = iota
	Learning
	Vocabulary
	Stable
)

func (ft BinType) String() string {
	switch ft {
	case Vocabulary:
		return "vocabulary"
	case Learning:
		return "learning"
	case Stable:
		return "stable"
	case New:
		return "new"
	}
	return "n/a"
}

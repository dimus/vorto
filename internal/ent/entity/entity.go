package entity

import (
	"time"
)

// CardStack contains information about the set of cards. It includes
// all possible types of cards, for example Learning, Vocabulary, etc.
type CardStack struct {
	Set  string `json:"set"`
	Bins `json:"bins"`
}

// Card contains information about the card.
type Card struct {
	ID       string   `json:"id"`
	Val      string   `json:"value"`
	Def      string   `json:"defenition"`
	Examples []string `json:"examples,omitempty"`
	Reply    `json:"replies"`
	// Weight is used to select cards from the set.
	Weight float64
}

// Reply contains information about answers and the last answer time.
type Reply struct {
	// Answers used as a binary where 1 is good and 0 is bad answers.
	// The last binary digit is the last answer. For example
	// 000101 means 'good, bad, good, bad, bad, bad'
	Answers   uint64 `json:"answers"`
	TimeStamp int32  `json:"ts"`
}

// Days returns the number of days since the last answer.
func (card *Card) Days() int64 {
	t := time.Now().Unix()
	var diff int64
	if card.TimeStamp > 0 {
		diff = t - int64(card.TimeStamp)
	}
	return diff / 86400
}

// GoodAnswers returns the number of good answers.
func (card *Card) GoodAnswers() int {
	var res int
	ans := card.Answers
	for ans > 0 {
		// wrong answers  have 0 as the last binary character
		// and are divisible by 2
		if ans%2 == 0 {
			break
		}
		res += 1
		ans = ans >> 1
	}
	return res
}

func boolToUint64(b bool) uint64 {
	if b {
		return 1
	} else {
		return 0
	}
}

// RegisterReply adds successful answer to the card if iSuccess is true,
// otherwize nullifies the answers.
func (rs Reply) RegisterReply(isSuccess bool) Reply {
	return Reply{
		Answers:   rs.Answers<<1 + boolToUint64(isSuccess),
		TimeStamp: int32(time.Now().Unix()),
	}
}

// LastGoodAnsw returns the number of good answers.
func (rs Reply) LastGoodAnsw() int {
	var res int
	ans := rs.Answers
	for ans > 0 {
		if ans%2 == 0 {
			break
		}
		res++
		ans = ans >> 1
	}
	return res
}

type Bins map[BinType][]*Card

package loader

import "codeberg.org/dimus/vorto/internal/ent/entity"

// Loader takes care of taking data from storage and saving data to storage.
type Loader interface {
	// Init creates infrastructure of the storage if it is missing.
	Init() error

	// Load takes data from the storage.
	Load(set string) (*entity.CardStack, error)

	// Save puts data into storage
	Save(cards *entity.CardStack) error
}

package scorer

import "github.com/gnames/levenshtein/ent/editdist"

type score struct{}

func New() Scorer {
	return score{}
}

// Score returns a score for a card value and an answer.
func (s score) Score(val, answer string) (int, int) {
	badScore := -100
	score := badScore
	ed, _ := editdist.ComputeDistanceMax(val, answer, 4)

	if ed < 4 {
		score = ed * -1
	}

	if score == badScore {
		return -1, ed
	}

	if score < 0 {
		return 0, ed
	}

	return 1, ed
}

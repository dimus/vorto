package scorer

// Scorer deals with assigning a score to a learning event.
type Scorer interface {
	// Score compares an answer and the correct answer and calculates the score.
	Score(value, answer string) (int, int)
}

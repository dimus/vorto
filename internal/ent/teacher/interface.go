package teacher

import (
	"codeberg.org/dimus/vorto/internal/ent/entity"
)

// Teacher provides methods to learn new words and to quiz for words that are
// already learned.
type Teacher interface {
	// Train is for learning new words.
	Train(vocab entity.BinType, isQuiz bool, count int)

	// AskSpelling asks to spell the exact value of a card.
	// It returns true if the answer was correct.
	AskSpelling(card *entity.Card, withSecondChance bool) bool

	// AskMeaning asks to remember the meaning of a card.
	// It returns true if the answer was correct.
	AskMeaning(card *entity.Card) bool
}

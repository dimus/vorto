package teacher

import (
	"log"
	"math/rand"
	"strings"
	"time"

	"codeberg.org/dimus/vorto/internal/ent/cardsel"
	"codeberg.org/dimus/vorto/internal/ent/entity"
	"codeberg.org/dimus/vorto/internal/ent/output"
	"codeberg.org/dimus/vorto/internal/ent/scorer"
)

var (
	r = rand.New(rand.NewSource(time.Now().UnixNano()))
)

type teacher struct {
	out output.Output
	*entity.CardStack
	scorer.Scorer
	Score int
}

// New creates a new teacher.
func New(cs *entity.CardStack, out output.Output) Teacher {
	return teacher{
		out:       out,
		CardStack: cs,
		Scorer:    scorer.New(),
	}
}

// Train uses the cards in the given bin to train the user.
func (t teacher) Train(bin entity.BinType, isQuiz bool, count int) {
	var ok bool
	var cards []*entity.Card
	cs := cardsel.New()

	t.out.Header(isQuiz)

	if cards, ok = t.Bins[bin]; !ok || len(cards) <= 0 {
		log.Printf("There are no cards in a '%s' bin.", bin)
	} else {
		cards = cs.SelectCards(cards)
		if count > 0 && len(cards) > count {
			cards = cards[0:count]
		}
		vocabCand := t.runExam(cards, isQuiz)

		if len(vocabCand) > 0 {
			t.runFinalExam(vocabCand)
		}
	}
}

// AskSpelling asks to spell the exact value of a card.
// It returns true if the answer was correct.
func (t teacher) AskSpelling(
	card *entity.Card,
	withSecondChance bool,
) bool {
	var tries int
	for {
		tries++
		text := t.out.EnquireVal(card)
		text = normPhrase(text)
		score, ed := t.Scorer.Score(text, card.Val)
		showEx := t.out.ScoreResult(card, text, score, ed, withSecondChance)
		if showEx {
			t.out.ShowExamples(card)
		}

		// for vocabulary give one more chance if answer was wrong
		if score < 1 && withSecondChance {
			return t.AskSpelling(card, false)
		} else if score == 1 {
			if tries == 1 {
				card.Reply = card.RegisterReply(true)
				return true
			}
			card.Reply = card.RegisterReply(false)
			return false
		}
	}
}

// AskMeaning shows the value of a card and asks for the definition.
// If user thinks he knows the answer, the card is marked with a good answer.
// Returns true if the card is learned, otherwise false.
func (t teacher) AskMeaning(card *entity.Card) bool {
	isLearned := t.out.EnquireDef(card)
	card.Reply = card.RegisterReply(isLearned)
	t.out.ShowExamples(card)
	return isLearned
}

func normPhrase(s string) string {
	el := strings.Split(s, " ")
	var res []string
	for _, v := range el {
		if v == "" {
			continue
		}
		res = append(res, v)
	}
	return strings.Join(res, " ")
}

func (t teacher) runFinalExam(cards []*entity.Card) {
	t.out.FinalExamTitle(cards)
	for i, card := range cards {
		t.out.ExamHeader(cards, i)
		t.AskMeaning(card)
		t.out.ExamFooter(card, false)
	}
}

func (t teacher) runExam(cards []*entity.Card, isQuiz bool) []*entity.Card {
	var vocabCand []*entity.Card
	coin := flipCoin(0.1)
	for i, card := range cards {
		t.out.ExamHeader(cards, i)
		if coin && isQuiz {
			// meaning quiz
			t.AskMeaning(card)
		} else {
			// spelling quiz
			t.AskMeaning(card)
			// t.AskSpelling(card, isQuiz)
		}
		t.out.ExamFooter(card, isQuiz)

		// This can only be true for cards from Learnings bin.
		if card.GoodAnswers() == 3 {
			vocabCand = append(vocabCand, card)
		}
	}
	return vocabCand
}

// flipCoin returns true with a given probability.
func flipCoin(prob float64) bool {
	return prob > 0 && prob < r.Float64()
}

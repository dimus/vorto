package output

import "codeberg.org/dimus/vorto/internal/ent/entity"

// Output is an interface for outputting data to the user.
type Output interface {
	// Header is the header of for a question.
	Header(bool)

	// EnquireVal prints out the question.
	EnquireVal(*entity.Card) string

	// EnquireDef shows the value of the card and asks if user remembers it.
	EnquireDef(*entity.Card) bool

	// ScoreResult prints out results of the answer. It returns true if
	// examples can be shown.
	ScoreResult(*entity.Card, string, int, int, bool) bool

	// ShowExamples shows examples of the card.
	ShowExamples(*entity.Card)

	// FinalExamTitle marks the beginning of a final exam.
	FinalExamTitle([]*entity.Card)

	// ExamHeader is the header for the whole quiz.
	ExamHeader([]*entity.Card, int)

	// ExamFooter is the footer for the whole quiz.
	ExamFooter(*entity.Card, bool)

	// OldCardsExam declares reviewing of oldest cards.
	OldCardsExam()
}

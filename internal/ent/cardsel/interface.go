package cardsel

import "codeberg.org/dimus/vorto/internal/ent/entity"

// CardSel is an interface for selecting cards from a set.
type CardSel interface {
	// SelectCards selects cards from a set.
	SelectCards([]*entity.Card) []*entity.Card
}

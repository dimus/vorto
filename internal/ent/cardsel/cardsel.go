package cardsel

import (
	"cmp"
	"fmt"
	"math/rand"
	"slices"
	"strconv"
	"time"

	"codeberg.org/dimus/vorto/internal/ent/entity"
	"github.com/fatih/color"
)

var (
	cardsPerQuiz            = 25
	r            *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
	maxDays      int64      = 15
	// the larger weightCoef, the more results has higher weights
	weightCoef float64 = 2
	// the larger answersCoef, the more important are answers
	answersCoef float64 = 1
	// the larger daysCoef, the less important are days
	daysCoef float64 = 15
)

type cardsel struct {
}

func New() CardSel {
	return cardsel{}
}

// SelectCards selects required number of cards from a set.
func (csl cardsel) SelectCards(cs []*entity.Card) []*entity.Card {
	shuffleCards(cs)
	if len(cs) == 0 {
		return cs
	}
	if len(cs) <= cardsPerQuiz {
		return cs
	}

	old, cs := oldAndNewCards(cs)
	cardsNum := cardsPerQuiz - len(old)

	res := selCards(cs, cardsNum)
	res = append(res, old...)
	slices.SortFunc(res, func(a, b *entity.Card) int {
		return cmp.Compare(a.Weight, b.Weight)
	})
	stats := make(map[int]int)
	for i := range res {
		stats[res[i].GoodAnswers()] += 1
	}
	showQuizStats(stats)

	shuffleCards(res)
	return res
}

func selCards(cs []*entity.Card, num int) []*entity.Card {
	calcWeight(cs)

	return weightedRand(cs, num)
}

func weightedRand(cs []*entity.Card, num int) []*entity.Card {
	var res []*entity.Card
	var cumulativeSum float64
	for _, v := range cs {
		cumulativeSum += v.Weight
	}

	// Sort cs by Weight, ascending order
	slices.SortFunc(cs, func(a, b *entity.Card) int {
		return cmp.Compare(a.Weight, b.Weight)
	})

	for i := 0; i < num; i++ {
		// Generate a random value between 0 and the total cumulative sum
		randomValue := r.Float64() * cumulativeSum

		// Iterate through the sorted items and subtract each item's weight from randomValue
		// When randomValue becomes negative, select the corresponding item
		for j, item := range cs {
			randomValue -= item.Weight
			if randomValue <= 0 {
				res = append(res, item)
				cumulativeSum -= item.Weight
				// remove selected card from cs
				cs = append(cs[:j], cs[j+1:]...)
				break
			}
		}
	}
	return res
}

func calcWeight(cs []*entity.Card) {
	useDays := r.Float32() > 0.75
	for i := range cs {
		ansNum := float64(cs[i].GoodAnswers())
		days := float64(cs[i].Days())

		if ansNum < 3 {
			cs[i].Weight = r.Float64()
			continue
		}

		ans := answersCoef / ansNum
		wgt := ans
		_ = days
		if useDays {
			// ds = days
			wgt = 1 / wgt
		}
		// wgt := ans * ds
		cs[i].Weight = wgt
	}
	if len(cs) == 0 {
		return
	}
	normalizeWeight(cs)
	// for i := range cs {
	// 	cs[i].Weight = math.Pow(cs[i].Weight, weightCoef)
	// }
}

func normalizeWeight(cs []*entity.Card) {
	minCrd := slices.MinFunc(cs, func(a, b *entity.Card) int {
		return cmp.Compare(a.Weight, b.Weight)
	})
	maxCrd := slices.MaxFunc(cs, func(a, b *entity.Card) int {
		return cmp.Compare(a.Weight, b.Weight)
	})
	if minCrd.Weight == maxCrd.Weight {
		return
	}
	targMin := 0.01
	targMax := 0.99
	scale := (targMax - targMin) / (maxCrd.Weight - minCrd.Weight)
	offset := targMin - minCrd.Weight*scale
	for i := range cs {
		cs[i].Weight = cs[i].Weight*scale + offset
	}
	minCrd = slices.MinFunc(cs, func(a, b *entity.Card) int {
		return cmp.Compare(a.Weight, b.Weight)
	})
	maxCrd = slices.MaxFunc(cs, func(a, b *entity.Card) int {
		return cmp.Compare(a.Weight, b.Weight)
	})
}

// oldAndNewCards returns cards that were not in circulation for long time,
// or have not been answered much.
func oldAndNewCards(cs []*entity.Card) ([]*entity.Card, []*entity.Card) {
	var res, resCs []*entity.Card

	var maxCardsNum = 15
	var count int
	for i := range cs {
		if cs[i].Days() > maxDays {
			count++
			res = append(res, cs[i])
		} else if cs[i].Days() > 1 && cs[i].GoodAnswers() < 4 {
			count++
			res = append(res, cs[i])
		} else if cs[i].GoodAnswers() < 15 {
			resCs = append(resCs, cs[i])
		}
	}
	if len(res) > maxCardsNum {
		res = res[0:maxCardsNum]
	}
	return res, resCs
}

func shuffleCards(cards []*entity.Card) {
	r.Shuffle(len(cards), func(i, j int) {
		cards[i], cards[j] = cards[j], cards[i]
	})
}

func showQuizStats(stats map[int]int) {
	fmt.Print("\nDistribution of cards by the number of answers:\n\n")
	var ansNum []int
	for k := range stats {
		ansNum = append(ansNum, k)
	}
	slices.Sort(ansNum)
	for _, ansNum := range ansNum {
		cardsNum := stats[ansNum]
		fmt.Printf("Answers: %s, Cards: %s\n",
			color.GreenString(strconv.Itoa(ansNum)),
			color.GreenString(strconv.Itoa(cardsNum)),
		)
	}
	fmt.Println()
}

package loaderio

import (
	"fmt"
	"os"

	"path/filepath"

	"github.com/gnames/gnsys"
)

// Init creates the necessary files and directories for the app to work.
func (e loaderio) Init() error {
	err := e.touchWorkDirs()
	if err != nil {
		return err
	}

	err = e.touchFiles()
	if err != nil {
		return err
	}

	err = e.initJSON()
	if err != nil {
		return err
	}

	return nil
}

func (e loaderio) touchWorkDirs() error {
	dir := e.DataDir
	cardsDir := filepath.Join(dir, "flashcards")
	for set := range e.CardsSets {
		setDir := filepath.Join(cardsDir, set)
		err := gnsys.MakeDir(setDir)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e loaderio) touchFiles() error {
	dir := e.DataDir
	cardsDir := filepath.Join(dir, "flashcards")

	for set := range e.CardsSets {
		setDir := filepath.Join(cardsDir, set)
		for _, file := range cardBins {
			file := file.String() + ".txt"
			filePath := filepath.Join(setDir, file)
			exists, _ := gnsys.FileExists(filePath)
			if !exists {
				fmt.Printf("Creating file %s\n", filePath)
				f, err := os.Create(filePath)
				if err != nil {
					return err
				}
				f.Close()
			}
		}
		err := e.touchCardStackFile(setDir)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e loaderio) touchCardStackFile(dir string) error {
	filePath := filepath.Join(dir, "card-stack.txt")
	exists, _ := gnsys.FileExists(filePath)
	if exists {
		return nil
	}

	text := "type = general\n"
	err := os.WriteFile(filePath, []byte(text), 0644)
	return err
}

func (e loaderio) initJSON() error {
	dir := e.DataDir
	cardsDir := filepath.Join(dir, "flashcards")

	for set := range e.CardsSets {
		setDir := filepath.Join(cardsDir, set)
		fileJSONPath := filepath.Join(setDir, e.FileJSON)
		exists, _ := gnsys.FileExists(fileJSONPath)
		if exists {
			continue
		}
		var emptyCardStorage []CardStorage
		csJSON, err := e.Encoder.Encode(emptyCardStorage)
		if err != nil {
			return err
		}
		err = os.WriteFile(fileJSONPath, csJSON, 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

package loaderio

import (
	"cmp"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"time"

	"codeberg.org/dimus/vorto/internal/ent/entity"
	"github.com/fatih/color"
)

var r *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))

// CardStorage is a struct that is used to save cards into vorto.json file.
type CardStorage struct {
	Value        string `json:"value"`
	entity.Reply `json:"data"`
}

// Save saves a set of cards into files.
func (e loaderio) Save(cs *entity.CardStack) error {
	// go through stable, see if there is anything without answer.
	toLearnS, stable := partitionStable(cs)
	// go through vocabulary, get ones without answers, then ones
	// with more than 9 answers, the rest stays in vocabulary.
	toLearnV, toStable, vocab := partitionVocabulary(cs)
	// in learning promote everything with more than 2 answers to
	// vocabulary.
	toVocab, learn := partitionLearning(cs)

	toLearnV = append(toLearnV, toLearnS...)

	cs.Bins[entity.Stable] = append(stable, toStable...)
	cs.Bins[entity.Vocabulary] = append(vocab, toVocab...)
	cs.Bins[entity.Learning] = append(learn, toLearnV...)

	newWordsNum := 25 - len(cs.Bins[entity.Learning])
	addNewWords(cs, newWordsNum)

	e.writeToFiles(cs)

	color.Green("\n\nStable has %d words.\n", len(cs.Bins[entity.Stable]))
	color.Green("Vocabulary has %d words.\n", len(cs.Bins[entity.Vocabulary]))
	color.Green("Learning has %d words.\n", len(cs.Bins[entity.Learning]))

	return nil
}

// goes to new.txt and picks more words to learn.
func addNewWords(cs *entity.CardStack, num int) {
	if num <= 15 {
		return
	}

	if num > len(cs.Bins[entity.New]) {
		num = len(cs.Bins[entity.New])
	}

	shuffleCards(cs.Bins[entity.New])
	newLearn := cs.Bins[entity.New][0:num]
	color.Green("\nMoving from 'new' to 'learning':")
	for i, v := range newLearn {
		color.Green("%d. %s", i, v.Val)
	}
	cs.Bins[entity.Learning] = append(cs.Bins[entity.Learning],
		newLearn...)
	cs.Bins[entity.New] = cs.Bins[entity.New][num:]
}

func shuffleCards(cards []*entity.Card) {
	r.Shuffle(len(cards), func(i, j int) {
		cards[i], cards[j] = cards[j], cards[i]
	})
}

func partitionStable(
	cs *entity.CardStack,
) (toLearn, stable []*entity.Card) {
	for _, c := range cs.Bins[entity.Stable] {
		if c.GoodAnswers() < 10 {
			var reply entity.Reply
			c.Reply = reply
			toLearn = append(toLearn, c)
		} else {
			stable = append(stable, c)
		}
	}
	if len(toLearn) > 0 {
		color.Red("\nMoving from 'stable' to 'learning':")
	}
	for i, v := range toLearn {
		color.Red("%d. %s\n", i+1, v.Val)
	}

	return toLearn, stable
}

func partitionVocabulary(
	cs *entity.CardStack,
) (toLearn, toStable, vocab []*entity.Card) {
	voc := cs.Bins[entity.Vocabulary]
	vocLen := len(voc)
	for _, c := range voc {
		if c.GoodAnswers() >= 10 && vocLen > 250 {
			toStable = append(toStable, c)
		} else if c.Answers > 0 && c.Answers%2 == 0 {
			var reply entity.Reply
			c.Reply = reply
			toLearn = append(toLearn, c)
		} else {
			vocab = append(vocab, c)
		}
	}

	if len(toLearn) > 0 {
		color.Red("\nMoving from 'vocabulary' to 'learning':")
	}
	for i, v := range toLearn {
		color.Red("%d. %s\n", i+1, v.Val)
	}

	if len(toStable) > 0 {
		color.Green("\nMoving from 'vocabulary' to 'stable':")
	}
	for i, v := range toStable {
		color.Green("%d. %s\n", i+1, v.Val)
	}

	return toLearn, toStable, vocab
}

func partitionLearning(cs *entity.CardStack) ([]*entity.Card, []*entity.Card) {
	var toVocab, learn []*entity.Card
	for _, c := range cs.Bins[entity.Learning] {
		if c.LastGoodAnsw() > 2 {
			c.Answers = 7 // 111
			toVocab = append(toVocab, c)
		} else {
			learn = append(learn, c)
		}
	}
	if len(toVocab) > 0 {
		color.Green("\nMoving from learning to vocabulary:")
	}
	for i, v := range toVocab {
		color.Green("%d. %s\n", i+1, v.Val)
	}
	return toVocab, learn
}

func (e loaderio) writeToFiles(cs *entity.CardStack) error {
	var err error
	cMap := make(map[string]entity.Reply)
	bins := []entity.BinType{
		entity.Learning,
		entity.Vocabulary,
		entity.Stable,
		entity.New,
	}

	for _, bin := range bins {
		cMap, err = e.saveBin(cs, bin, cMap)
		if err != nil {
			return err
		}
	}

	// save metadata into vorto.json file.
	path := filepath.Join(e.DataDir, "flashcards", cs.Set, e.FileJSON)
	return e.saveCardMap(path, cMap)
}

func (e loaderio) saveBin(
	cs *entity.CardStack,
	bin entity.BinType,
	m cardMap,
) (cardMap, error) {

	cMap := make(map[string]entity.Reply)
	for k, v := range m {
		cMap[k] = v
	}
	path := filepath.Join(e.DataDir, "flashcards", cs.Set, bin.String()+".txt")
	var cards []string

	slices.SortFunc(cs.Bins[bin], func(a, b *entity.Card) int {
		return cmp.Compare(a.Val, b.Val)
	})
	for _, card := range cs.Bins[bin] {
		if _, ok := cMap[card.Val]; ok {
			continue
		} else {
			cMap[card.Val] = card.Reply
			cards = append(cards, fmt.Sprintf("%s = %s", card.Val, card.Def))
			for _, v := range card.Examples {
				cards = append(cards, ": "+v)
			}
		}
	}

	err := os.WriteFile(path, []byte(strings.Join(cards, "\n")), 0664)
	if err != nil {
		return nil, err
	}
	return cMap, nil
}

func (e loaderio) saveCardMap(path string, m cardMap) error {
	storage := make([]CardStorage, 0, len(m))
	for k, v := range m {
		storage = append(storage, CardStorage{Value: k, Reply: v})
	}
	slices.SortStableFunc(storage, func(a, b CardStorage) int {
		return cmp.Compare(a.Value, b.Value)
	})
	res, err := e.Encoder.Encode(storage)
	if err != nil {
		return err
	}
	return os.WriteFile(path, res, 0644)
}

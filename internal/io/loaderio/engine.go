package loaderio

import (
	"codeberg.org/dimus/vorto/internal/config"
	"codeberg.org/dimus/vorto/internal/ent/entity"
	"codeberg.org/dimus/vorto/internal/ent/loader"
	"github.com/gnames/gnfmt"
)

var cardBins = []entity.BinType{
	entity.Stable,
	entity.Vocabulary,
	entity.Learning,
	entity.New,
}

type loaderio struct {
	config.Config
	FileJSON string
	Encoder  gnfmt.GNjson
}

// New returns a new instance of Loader.
func New(cfg config.Config) loader.Loader {
	return loaderio{
		Config:   cfg,
		FileJSON: "vorto.json",
		Encoder:  gnfmt.GNjson{Pretty: true},
	}
}

// cardMap is a map of a card value to card replies.
type cardMap map[string]entity.Reply

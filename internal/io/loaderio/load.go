package loaderio

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"time"

	"codeberg.org/dimus/vorto/internal/ent/entity"
	"github.com/gnames/gnuuid"
)

// Load loads a set of cards from a file.
func (e loaderio) Load(set string) (*entity.CardStack, error) {
	// initiate empty card stack
	res, err := e.initCardStack(set)
	if err != nil {
		return &res, err
	}

	// fill card stack with data.
	err = e.populateCardStack(&res)
	if err != nil {
		return &res, err
	}
	return &res, nil
}

// initCardStack finds a set and returns empty CardStack of the set.
func (e loaderio) initCardStack(set string) (entity.CardStack, error) {
	res := entity.CardStack{
		Bins: make(map[entity.BinType][]*entity.Card),
	}
	if _, ok := e.CardsSets[set]; !ok {
		var sets []string
		for k := range e.Config.CardsSets {
			sets = append(sets, k)
		}
		slices.Sort(sets)
		fmt.Printf("\nSets include '%s'\n\n", strings.Join(sets, ", "))
		return res, fmt.Errorf("set '%s' is not in Sets", set)
	}
	res.Set = set
	return res, nil
}

func (e loaderio) populateCardStack(cs *entity.CardStack) error {
	dir := e.DataDir
	cardsDir := filepath.Join(dir, "flashcards")
	setDir := filepath.Join(cardsDir, cs.Set)

	for _, file := range cardBins {
		err := e.loadFile(setDir, file, cs)
		if err != nil {
			return err
		}
	}
	return nil
}

func (e loaderio) savedCardMap(set string) (cardMap, error) {
	var res []CardStorage
	var cm cardMap = make(map[string]entity.Reply)
	filePath := filepath.Join(e.DataDir, "flashcards", set, e.FileJSON)
	text, err := os.ReadFile(filePath)
	if err != nil {
		return cm, err
	}
	err = e.Encoder.Decode(text, &res)
	for _, v := range res {
		cm[v.Value] = v.Reply
	}
	return cm, err
}

func (e loaderio) prepareDataJSON() {
}

// parseLine takes a string and breaks it into two fields if it detects
// ' = ' pattern. It ignores comments designated by leading '#' character
// and empty lines. It also looks for examples, starting with ':'.
// It returns 3 fields, and a boolean that indicates
// a success or failure of parsing.
// Fields: answer, question, example, success
func parseLine(line string) (string, string, string, bool) {
	if strings.HasPrefix(line, ":") {
		line = strings.TrimLeft(line, ":")
		line = strings.TrimSpace(line)
		return "", "", line, true
	}
	fields := strings.Split(line, " = ")
	if len(fields) != 2 {
		return "", "", "", false
	}

	field1 := strings.Trim(fields[0], " ")
	field2 := strings.Trim(fields[1], " ")
	if len(field1) < 1 || len(field2) < 1 || field1[0] == '#' {
		return "", "", "", false
	}
	return field1, field2, "", true
}

func (e loaderio) loadFile(dir string, bin entity.BinType, cs *entity.CardStack) error {
	ts := int32(time.Now().Unix())
	oldCardMap, err := e.savedCardMap(cs.Set)
	if err != nil {
		return err
	}
	filePath := filepath.Join(dir, bin.String()+".txt")
	f, err := os.OpenFile(filePath, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return err
	}
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		line := sc.Text()
		val, def, eg, success := parseLine(line)
		if !success {
			continue
		}

		if eg != "" {
			l := len(cs.Bins[bin])
			if l == 0 {
				continue
			}
			card := cs.Bins[bin][l-1]
			card.Examples = append(card.Examples, eg)
			continue
		}

		card := entity.Card{ID: gnuuid.New(val).String(), Val: val, Def: def}
		if reply, ok := oldCardMap[card.Val]; ok {
			card.Reply = reply
		}
		if bin == entity.Vocabulary && card.Answers == 0 {
			card.Answers = 31 // binary 11111
			card.TimeStamp = ts
		}

		cs.Bins[bin] = append(cs.Bins[bin], &card)
	}
	return nil
}

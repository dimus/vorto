package outputio

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"

	"codeberg.org/dimus/vorto/internal/ent/entity"
	"codeberg.org/dimus/vorto/internal/ent/output"
	"github.com/fatih/color"
	"github.com/gnames/levenshtein/ent/editdist"
)

var rgParen = regexp.MustCompile(`\([^\(\)]+\)`)

type outputio struct {
}

// New creates a new output.
func New() output.Output {
	return outputio{}
}

// OldCardsExam shows the beginning of an exam for oldest cards.
func (o outputio) OldCardsExam() {
	color.Green("reviewing oldest cards...\n")
}

// Header shows the beginning of an exam.
func (o outputio) Header(isQuiz bool) {
	if isQuiz {
		color.Green("\nchecking learned before cards...\n")
	} else {
		color.Yellow("learning new cards...")
	}
}

// FinalExamTitle marks the beginning of a final exam.
func (o outputio) FinalExamTitle(cards []*entity.Card) {
	line()
	fmt.Printf("\n\n%s\n\n",
		color.RedString("The Final Exam for %d cards!\nGood luck!", len(cards)),
	)
}

func line() {
	fmt.Println("------------------------------------------------------------")
}

// ExamHeader shows the beginning of a question.
func (o outputio) ExamHeader(cards []*entity.Card, i int) {
	line()
	fmt.Printf("%d out of %d\n", i+1, len(cards))
}

// ExamFooter shows the end of a question.
func (o outputio) ExamFooter(card *entity.Card, isQuiz bool) {
	if isQuiz {
		fmt.Printf("past answers: %s\n", quizAnswersStr(card))
	} else {
		fmt.Printf("past answers: %s\n", answersStr(card))
	}
}

// EnquireVal asks the user to input the answer to the question.
// The answer should be a correct value of the card.
func (o outputio) EnquireVal(card *entity.Card) string {
	fmt.Printf("Spell this: %s\n", question(card.Def))
	answ := colorAnswers(card.GoodAnswers())
	fmt.Printf("(%s, %d days ago)\n", answ, card.Days())
	fmt.Print("-> ")
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text)
	return text
}

// EnquireDef asks if the user remembers the value of the card.
// User can answer 'yes' or 'no'. If the answer was 'yes',
// return true, otherwise false.
func (o outputio) EnquireDef(card *entity.Card) bool {
	fmt.Println("Try to remember description, then press Enter")
	fmt.Printf(
		"\n%s: %s\n",
		color.YellowString("Remember this?"),
		color.GreenString(card.Val),
	)

	o.ShowExamples(card)

	answ := colorAnswers(card.GoodAnswers())
	fmt.Printf("(%s, %d days ago)\n", answ, card.Days())

	fmt.Printf("Type %s to see the answer: ->",
		color.YellowString("(y,Enter/n)"))

	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	text = strings.ToLower(strings.TrimSpace(text))
	fmt.Printf("\n%s:         %s\n\n",
		color.YellowString("Answer"),
		color.GreenString(card.Def),
	)
	if text == "no" || text == "n" {
		return false
	}

	fmt.Printf("Does it look familiar? %s: ->\n", color.YellowString("(y,Enter/n)"))
	text, _ = reader.ReadString('\n')
	text = strings.ToLower(strings.TrimSpace(text))
	if strings.HasPrefix(text, "y") || len(text) == 0 {
		return true
	}
	return false
}

func colorAnswers(a int) string {
	res := strconv.Itoa(a) + " answers"
	switch a {
	case 1:
		return color.YellowString(res)
	case 0:
		return color.RedString(res)
	default:
		return color.GreenString(res)
	}
}

// ScoreResult shows the result of the question.
// It returns true if it is ok to show examples of the card.
func (o outputio) ScoreResult(
	card *entity.Card,
	input string,
	score int,
	ed int,
	withSecChance bool) bool {
	if score < 1 && withSecChance {
		msg := "Nope, one more chance"
		if ed < 4 {
			msg += fmt.Sprintf(" (ed %d)", ed)
		}
		color.Yellow(msg)
		return false
	}

	switch score {
	case 1:
		color.Green("-> %s", card.Val)
		color.Green("Correct!")
	case 0:
		_, _, res := editdist.ComputeDistanceTerm(input, card.Val)
		fmt.Println(color.YellowString("~> ") + res)
	default:
		color.Red("!> %s", card.Val)
	}
	return true
}

func (o outputio) ShowExamples(card *entity.Card) {
	if len(card.Examples) == 0 {
		return
	}
	fmt.Println()
	for _, v := range card.Examples {
		color.Cyan("E.g.: %s", v)
	}
	fmt.Println()
}

func quizAnswersStr(card *entity.Card) string {
	goods := card.GoodAnswers()
	if goods == 0 {
		return wrong()
	}
	res := fmt.Sprintf("%d rights", goods)
	return color.GreenString(res)
}

func wrong() string {
	return color.RedString("wrong")
}

func right() string {
	return color.GreenString("right")
}

func answersStr(card *entity.Card) string {
	ans := card.Answers
	if ans == 0 {
		return wrong()
	}
	var count int
	var res []string
	for ans > 0 && count < 5 {
		v := right()
		if ans%2 == 0 {
			v = wrong()
		}
		res = append(res, v)
		count += 1
		ans = ans >> 1
	}
	return strings.Join(res, ", ")
}

func question(s string) string {
	if !strings.Contains(s, "(") {
		return s
	}
	// color the text in parentheses yellow
	return rgParen.ReplaceAllString(s, "\033[1;33m${0}\033[0m")
}

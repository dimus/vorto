VERSION = $(shell git describe --tags)
VER = $(shell git describe --tags --abbrev=0)
DATE = $(shell date -u '+%Y-%m-%d_%H:%M:%S%Z')
NO_C = CGO_ENABLED=0
FLAGS_SHARED = $(NO_C) GOARCH=amd64
FLAGS_LD=-ldflags "-X codeberg.org/dimus/vorto/pkg.Build=${DATE} \
                  -X codeberg.org/dimus/vorto/pkg.Version=${VERSION}"
GOCMD=go
GOINSTALL=$(GOCMD) install $(FLAGS_LD)
GOBUILD=$(GOCMD) build $(FLAGS_LD)
GOCLEAN=$(GOCMD) clean
GOGET = $(GOCMD) get

all: install

test: deps install
	$(NO_C) go test ./...

tools: deps
	@echo Installing tools from tools.go
	@cat  tools.go | grep _ | awk -F'"' '{print $$2}' | xargs -tI % go install %

deps:
	$(GOCMD) mod download; \

build:
	$(GOCLEAN); \
	$(NO_C) $(GOBUILD);

release:
	$(GOCLEAN); \
	$(NO_C) GOOS=linux $(GOBUILD); \
	tar zcvf /tmp/vorto-${VER}-linux.tar.gz vorto; \
	$(GOCLEAN);

install:
	$(NO_C) $(GOINSTALL);

help:
	@echo
	@echo Build commands:
	@echo "  make [all]     - Install vorto"
	@echo "  make install   - Install vorto"
	@echo "  make help      - This help"
	@echo

/*
Copyright © 2020 Dmitry Mozzherin <dmozzherin@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	_ "embed"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"codeberg.org/dimus/vorto/internal/config"
	"codeberg.org/dimus/vorto/internal/ent/entity"
	"codeberg.org/dimus/vorto/internal/io/loaderio"
	"codeberg.org/dimus/vorto/internal/io/outputio"
	vorto "codeberg.org/dimus/vorto/pkg"
	"github.com/spf13/cobra"

	"github.com/gnames/gnsys"
	"github.com/spf13/viper"
)

//go:embed vorto.yaml
var configYAML string

var (
	opts []config.Option
)

// cnf purpose is to achieve automatic import of data from the
// configuration file, if it exists.
type configData struct {
	DataDir      string
	CurrentCards string
	CardsSets    []string
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "vorto",
	Short: "A flash cards app to learn new words, facts or terms",
	Run: func(cmd *cobra.Command, _ []string) {
		if showVersionFlag(cmd) {
			os.Exit(0)
		}
		stable, _ := cmd.Flags().GetBool("stable-only")
		old, _ := cmd.Flags().GetBool("old-only")
		newOnly, _ := cmd.Flags().GetBool("new-only")
		if old || stable {
			newOnly = false
		}
		if stable {
			old = false
		}
		opts = append(
			opts,
			config.OptStableOnly(stable),
			config.OptOldOnly(old),
			config.OptNewOnly(newOnly),
		)

		opts = cardsFlag(cmd, opts)
		var cardsStack *entity.CardStack
		c := config.New(opts...)
		vrt := vorto.New(
			c,
			vorto.OptLoader(loaderio.New(c)),
			vorto.OptOutput(outputio.New()),
		)
		defer func() {
			err := vrt.Save(cardsStack)
			if err != nil {
				log.Fatalf("Could not save progress: %s\n", err)
			}
		}()

		err := vrt.Init()
		if err != nil {
			log.Fatalf("Cannot initiate vorto: %s.", err)
		}

		cardsStack, err = vrt.Load()
		if err != nil {
			log.Fatalf("Cannot load cards: %s.", err)
		}

		vrt.Run(cardsStack)
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// init is called by main.main(). It only needs to happen once to the rootCmd.
func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.Flags().BoolP("version", "V", false, "show app's version")
	rootCmd.Flags().BoolP("stable-only", "s", false, "run training only for stable words")
	rootCmd.Flags().BoolP("old-only", "o", false, "run training only for vocabulary words")
	rootCmd.Flags().BoolP("new-only", "n", false, "run training only for learning words")
	rootCmd.PersistentFlags().StringP("cards", "c", "", "set of cards to use")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	configFile := "vorto"

	// Find home directory.
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatalf("Cannot find home directory: %s.", err)
	}
	configDir := filepath.Join(home, ".config")

	// Search config in home directory with name ".gnames" (without extension).
	viper.AddConfigPath(configDir)
	viper.SetConfigName(configFile)

	configPath := filepath.Join(configDir, fmt.Sprintf("%s.yaml", configFile))
	touchConfigFile(configPath)

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Printf("Using config file: %s.\n", viper.ConfigFileUsed())
	}
	getOpts()
}

// getOpts gets options from config file.
func getOpts() {
	cfg := &configData{}
	err := viper.Unmarshal(cfg)
	if err != nil {
		log.Fatalf("Cannot deserialize config data: %s.", err)
	}

	if cfg.DataDir != "" {
		opts = append(opts, config.OptDataDir(cfg.DataDir))
	}

	if cfg.CurrentCards != "" {
		opts = append(opts, config.OptCurrentCards(cfg.CurrentCards))
	}

	if len(cfg.CardsSets) > 0 {
		opts = append(opts, config.OptCardsSets(cfg.CardsSets))
	}
}

// touchConfigFile checks if config file exists, and if not, it gets created.
func touchConfigFile(configPath string) {
	exists, _ := gnsys.FileExists(configPath)
	if exists {
		return
	}

	log.Printf("Creating config file: %s.", configPath)
	createConfig(configPath, configYAML)
}

// createConfig creates config file.
func createConfig(path, configText string) {
	err := gnsys.MakeDir(filepath.Dir(path))
	if err != nil {
		log.Fatalf("Cannot create dir %s: %s.", path, err)
	}

	err = os.WriteFile(path, []byte(configText), 0644)
	if err != nil {
		log.Fatalf("Cannot write to file %s: %s.", path, err)
	}
}

// showVersionFlag provides version and the build timestamp. If it returns
// true, it means that version flag was given.
func showVersionFlag(cmd *cobra.Command) bool {
	hasVersionFlag, err := cmd.Flags().GetBool("version")
	if err != nil {
		log.Fatalf("Cannot get version flag: %s.", err)
	}

	if hasVersionFlag {
		fmt.Printf("\nversion: %s\nbuild: %s\n\n", vorto.Version, vorto.Build)
	}
	return hasVersionFlag
}

// cardsFlag provides the set of cards to use in the practice.
// The "default" set is used if this flag is not given.
func cardsFlag(cmd *cobra.Command, opts []config.Option) []config.Option {
	cards, _ := cmd.Flags().GetString("cards")
	if cards != "" {
		opts = append(opts, config.OptCurrentCards(cards))
	}
	return opts
}

# vorto

Vorto is a sophisticated flash card learning app. It uses CLI (command line
interface).

Vorto in esperanto means `a Word`. I wrote this program originally to
learn new words in a language. However it can be used for any kind of facts
that need to be memorized.

The Vorto app allows to have tens of thousand cards separated into card decks
by topics. There is also a default card deck, it is envoked when no card deck
option is provided.

## What Vorto does

Vorto allows you to work with a large number of cards. It has two modes,
learning and reinforcement of already learned. Cards already learned appear
more often when they were not answered enough times. Cards that become
"trivial" to answer become hidden and appear only if they were answered last
time more than a month ago. This approach allows to keep thousands of cards in
operation and practice with all of them.

## Entering information

## Command Line Interface

## Flash card learning algorithm.

Each card deck contains 3 flash card bins, `New`, `Learning` and `Vocabulary`.
The `New` bin contains a que of the cards that the User wants to learn at some
point. The `Learning` bin contains cards that are currently in the learning
phase. The `Vocabulary` bin contains cards that had been learned already.

At first cards are put into the `New` bin. There is no limit how many cards are
in the bin. When program starts, the `Learning` bin is checked. If it has no
cards, 25 random cards are moved from `New` bin to `Learning`. Note that
`Learning` bin can be filled manually with unlimited number of cards. However,
every learning cycle picks only 25 cards from it. It is recommended to not
have more than 100 cards in the `Learning` bin, optimal amount is 25 cards.

Than a User gets a prompt, they are provided a queese from a random card in the
`Learning` bin. If User answers the question correctly, the card gets 1 point.
The queeses continue until 25 cards are drown (or as many cards as there are in
the `Learning` bin). The User runs Vorto several times, and when a card gets 3
correct answers it is moved to `Vocabulary` bin.

When the number of cards in `Learning` is less than 15, the second swarm of up
to 25 queeses comes from the `Vocabulary` bin. An algorithm picks cards from
`Vocabulary` not completely randomly. Cards that are freshly moved from
`Learning` have higher probability to appear, also "older" cards that had less
correct answers are preferred. If a card got more that 15 correct answers it
goes into a "hidden" state and does not get called anymore. However, if
such card was answered last time more than a month ago, it would appear in the
queese again.

If a card from `Vocabulary` did not get the correct answer, it moves back to
the `Learning` bin.

When `Learning` bin is emptied enough (has less than 9 cards), new random cards
move from the `New` bin to the `Learning` setting the number of cards in the
later up to 25.

If too many cards go from `Vocabulary` to `Learning` (more than 15 cards). Only
queeses from `Learning` appear until cards number drops below 15.

This algorithm allows to comfortably work with several thousand cards and
strenghen the knowledge about all of them.

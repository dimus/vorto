package vorto

import "codeberg.org/dimus/vorto/internal/ent/entity"

// Vorto provides methods to run a practice for learning new words.
type Vorto interface {
	// Init creates storage used by Vorto if it does not exist.
	Init() error

	// Load populates Vorto with existing data from storage.
	Load() (*entity.CardStack, error)

	// Run starts a practice session.
	Run(cs *entity.CardStack)

	// Save saves results of a practice.
	Save(cs *entity.CardStack) error

	// Stats returns back statistics about the CardStack
	Stats() error
}

package vorto

import (
	"fmt"
	"slices"

	"codeberg.org/dimus/vorto/internal/config"
	"codeberg.org/dimus/vorto/internal/ent/entity"
	"codeberg.org/dimus/vorto/internal/ent/loader"
	"codeberg.org/dimus/vorto/internal/ent/output"
	"codeberg.org/dimus/vorto/internal/ent/teacher"
)

var (
	// Version is the version of the Vorto app.
	Version, Build string
)

// Option is a function that configures a Vorto instance.
type Option func(*vorto)

// OptOutput sets the output. This approach allows to use interface
// instead of concrete types.
func OptOutput(o output.Output) Option {
	return func(v *vorto) {
		v.Output = o
	}
}

// OptLoader sets the loader. This approach allows to use interface
// instead of concrete types.
func OptLoader(l loader.Loader) Option {
	return func(v *vorto) {
		v.Loader = l
	}
}

// vorto is the main struct that implements the Vorto interface.
type vorto struct {
	// Config is the configuration.
	config.Config

	// Loader works with the file system.
	loader.Loader

	// Teacher is the contains the logic of the quiz.
	teacher.Teacher

	// Output is the creates the output of the quiz.
	output.Output
}

// New creates a new Vorto instance.
func New(cfg config.Config, opts ...Option) Vorto {
	res := vorto{
		Config: cfg,
	}
	for _, opt := range opts {
		opt(&res)
	}
	return res
}

// Init initializes files in the file system.
func (vrt vorto) Init() error {
	return vrt.Loader.Init()
}

// Load loads the cards from the file.
func (vrt vorto) Load() (*entity.CardStack, error) {
	return vrt.Loader.Load(vrt.CurrentCards)
}

// Run runs the quiz.
func (vrt vorto) Run(cs *entity.CardStack) {
	vrt.Teacher = teacher.New(cs, vrt.Output)

	if vrt.StableOnly {
		vrt.Train(entity.Stable, true, 0)
		return
	}

	if vrt.OldOnly {
		vrt.Train(entity.Vocabulary, true, 0)
		return
	}

	vrt.Train(entity.Learning, false, 0)
	if vrt.NewOnly {
		return
	}

	// do not run vocabulary quiz if there are too many words to learn
	var count int
	for _, c := range cs.Bins[entity.Learning] {
		if c.LastGoodAnsw() < 3 {
			count++
		}
	}

	count = 25 - count
	if count <= 0 {
		return
	}

	vrt.Train(entity.Vocabulary, true, count)
}

// Save saves the cards to the storage.
func (vrt vorto) Save(cs *entity.CardStack) error {
	return vrt.Loader.Save(cs)
}

func (vrt vorto) Stats() error {
	cards, err := vrt.Loader.Load(vrt.CurrentCards)
	if err != nil {
		return err
	}
	fmt.Printf("\nCards Set: %s\n", cards.Set)

	fmt.Println("\nLearning")
	lrn := cards.Bins[entity.Learning]
	fmt.Printf("  Words: %d\n", len(lrn))

	st := cards.Bins[entity.Stable]

	voc := cards.Bins[entity.Vocabulary]
	if len(voc) > 0 {
		fmt.Println("\nVocabulary")
		fmt.Printf("  Words: %d\n", len(voc))
		distrDays := accessDaysAgo(voc)
		var ary []int
		for k := range distrDays {
			ary = append(ary, int(k))
		}
		slices.Sort(ary)
		for _, v := range ary {
			item := distrDays[int64(v)]
			num := item.cardsNum
			av := float64(item.answers) / float64(item.cardsNum)
			fmt.Printf(
				"  %2d Days Old: %4d cards, %5.1f av. answers \n",
				v, num, av,
			)
		}
		fmt.Println("\n  Answers")
		distrAnsw := answersNum(voc)
		ary = ary[0:0]
		for k := range distrAnsw {
			ary = append(ary, int(k))
		}
		slices.Sort(ary)
		var lrn5 int
		for _, v := range ary {
			item := distrAnsw[int64(v)]
			num := item.cardsNum
			if v > 5 {
				lrn5 += num
			}
			d := float64(item.days) / float64(item.cardsNum)
			fmt.Printf(
				"  %2d Answers: %4d cards, %5.1f av. days \n",
				v, num, d,
			)
		}

		fmt.Printf("\n\nWords to learn: %d\n", len(lrn))
		fmt.Printf("Words in vocabulary: %d\n", len(voc))
		fmt.Printf("Words in stable: %d\n", len(st))
		fmt.Printf("Total: %d\n", len(voc)+len(st)+len(lrn))
		fmt.Printf("Total w/o learning: %d\n\n", len(voc)+len(st))
		fmt.Printf("Words with more than 5 answers: %d\n", lrn5+len(st))

	}

	return nil
}

type stat struct {
	cardsNum, answers, days int
}

func accessDaysAgo(cards []*entity.Card) map[int64]stat {
	res := make(map[int64]stat)
	for _, v := range cards {
		if stt, ok := res[v.Days()]; ok {
			stt.cardsNum += 1
			stt.answers += v.GoodAnswers()
			res[v.Days()] = stt
		} else {
			res[v.Days()] = stat{cardsNum: 1, answers: v.GoodAnswers()}
		}
	}
	return res
}

func answersNum(cards []*entity.Card) map[int64]stat {
	res := make(map[int64]stat)
	for _, v := range cards {
		ans := v.GoodAnswers()
		if stt, ok := res[int64(ans)]; ok {
			stt.cardsNum += 1
			stt.days += int(v.Days())
			res[int64(ans)] = stt
		} else {
			res[int64(ans)] = stat{cardsNum: 1, days: int(v.Days())}
		}
	}
	return res
}
